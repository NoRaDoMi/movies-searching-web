// consts cho TheMovieDb API
const apiKey = 'ac97cb80f550a85ea84a010c7c2bf37f';
const baseURL = 'https://api.themoviedb.org/3/';
let configData = null;
let baseImageURL = null;

// Ham lay thong tin config cua API
async function getConfig() {
	const url = `${baseURL}configuration?api_key=${apiKey}`;
	const resp = await fetch(url);
	const result = await resp.json();
	baseImageURL = result.images.secure_base_url;
	console.log(baseImageURL);
	configData = result.images;
	console.log(configData);
}

async function evtSubmit(e) {
	e.preventDefault();
	// Dùng promise để loading xong sẽ bắt đầu xử lí tìm kiếm phim
	modalLoaing().then(searchingMovies);
}

// Hàm tạo hiệu ứng loading khi searching
function modalLoaing() {
	$('.modal').modal('show');
	return new Promise(function(resolve, reject) {
		setTimeout(function() {
			$('.modal').modal('hide');
			resolve('hejsan');
		}, 1500);
	});
}

async function searchingMovies() {
	// Kiêm tra xem tìm kiếm phim theo tên phim (value = 1) hay theo tên diễn viên (value =2 )
	let s = document.getElementById('option_searching');
	let kind = s.options[s.selectedIndex].value;

	let strSearch;

	// Lấy query searching
	strSearch = $('form input').val();

	if (strSearch == '') {
		await topRatedMovies();
		return;
	}
	// Tìm kiếm phim theo tên phim
	if (kind === '1') {
		searchingMoviesByName(strSearch);
	}
	// Tìm kiếm phim theo tên diễn viên tham gia
	else {
		searchMoviesByCast(strSearch);
	}
}

// ------------- CHỨC NĂNG TOP RATED LIST MOVIE VÀ PHÂN TRANG -------/
let currentPageTopRated = 0;

function topRatedMovies() {
	$('form input').val('');

	$('#main').empty();

	$('#main').append(
		`<div id="top-rated-movies" class="row">
				<!-- Nơi in ra list movies -->
		</div>
			`
	);

	$('#main').append(`
	
		<nav aria-label="Page navigation example" class="w-100 mt-5" >
			<ul class="pagination justify-content-center" id="pagination_1">
				<li class="page-item disabled" id="previous-page">
					<a
						class="page-link"
						href="javascript:void(0)"
						tabindex="-1"
						aria-disabled="true"
						>Prev</a
					>
				</li>
				<li class="page-item " id="next-page">
					<a class="page-link" href="javascript:void(0)">Next</a>
				</li>
			</ul>
		</nav>
	`);

	getMoviesTopRatedPerPage(1);

	$(document).on(
		'click',
		'#pagination_1 li.current-page:not(.active)',
		function() {
			return getMoviesTopRatedPerPage(+$(this).text());
		}
	);
	$('#next-page').on('click', function() {
		return getMoviesTopRatedPerPage(currentPageTopRated + 1);
	});

	$('#previous-page').on('click', function() {
		return getMoviesTopRatedPerPage(currentPageTopRated - 1);
	});
	$('#pagination_1').on('click', function() {
		$('html,body').animate({ scrollTop: 0 }, 100);
	});
}

async function getMoviesTopRatedPerPage(whichPage) {
	const reqStr = `${baseURL}movie/top_rated?api_key=${apiKey}&page=${whichPage}`;
	const response = await fetch(reqStr);
	const movies = await response.json();

	currentPageTopRated = whichPage;

	showTopRatedMovies(movies);
}

// Hàm in list top rated movies theo trang
async function showTopRatedMovies(movies) {
	// In list top rated movies theo trang whcihPage
	$('#top-rated-movies').empty();
	let lengthMovie, imgUrl;
	for (const m of movies.results) {
		//lengthMovie = await getLengthOfMovie(m.id);
		if (m === undefined)
			imgUrl = `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw5WrlkUQT9BMeXPL4NVAwuD1n9hqczs__ff8Uv8XJnPfau98e&s`;
		else {
			if (m.poster_path === null) {
				imgUrl = `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw5WrlkUQT9BMeXPL4NVAwuD1n9hqczs__ff8Uv8XJnPfau98e&s`;
			} else {
				imgUrl = `${baseImageURL}w500/${m.poster_path}`;
			}
		}
		$('#top-rated-movies').append(
			`<div class="col-md-4 py-1 mt-2">
				<div class="card shadow h-100" onclick="movieDetails(${m.id})">
					<div class="about-picture">
						<img
							class="card-img-top"
							src="${imgUrl}"
							alt="Card image cap"
						/>
						<p class="picture-title">
							<i class="fa fa-play fa" aria-hidden="true"></i>
						</p>
					</div>
					
					<div class="card-body ">
						<h5 class="card-title "><a class="text-dark" href="#">${m.title}</a></h5>
						<div class="row">
						<div class="col-7">
						<p class="text-secondary pb-0 mb-0"> <span class="fa fa-star checked"></span>  ${m.vote_average}</p></div>
						<div class="col-5">
						<p class="text-secondary pb-0 mb-0 pl-3">
						<i class="fa fa-calendar pr-2" aria-hidden="true"></i>
						${m.release_date} </p>
						</div>
					</div>
					</div>	
				</div>
			</div>`
		);
	}

	// Xử lí cho phần pagination
	// Replace the navigation items (not prev/next):
	$('#pagination_1 li')
		.slice(1, -1)
		.remove();
	getPageList(
		movies.total_pages,
		currentPageTopRated,
		movies.results.length
	).forEach(item => {
		$('<li>')
			.addClass('page-item')
			.addClass(item ? 'current-page' : 'disabled')
			.toggleClass('active', item === currentPageTopRated)
			.append(
				$('<a>')
					.addClass('page-link')
					.attr({
						href: 'javascript:void(0)'
					})
					.text(item || '...')
			)
			.insertBefore('#next-page');
	});
	// Disable prev/next when at first/last page:
	$('#previous-page').toggleClass('disabled', currentPageTopRated === 1);
	$('#next-page').toggleClass(
		'disabled',
		currentPageTopRated === movies.total_pages
	);
}
// ---------- KẾT THÚC CHỨC NĂNG TOP RATED LIST MOVIE VÀ PHÂN TRANG --------//

// ------------- CHỨC NĂNG TÌM KIẾM MOVIE THEO TÊN MOVIE VÀ PHÂN TRANG -------//
async function searchingMoviesByName(strSearch) {
	$('#main').empty();
	$('#main').append(`
	<div id="movies-searching-by-name" class="row">
		<!-- Nơi in ra list movies searching by name-->
	</div>
	<nav aria-label="Page navigation example" class="w-100 mt-5" >
		<ul class="pagination justify-content-center" id="pagination_4">
			<li class="page-item disabled" id="previous-page-4">
				<a
					class="page-link"
					href="javascript:void(0)"
					tabindex="-1"
					aria-disabled="true"
					>Prev</a
				>
			</li>
			<li class="page-item " id="next-page-4">
				<a class="page-link" href="javascript:void(0)">Next</a>
			</li>
		</ul>
	</nav>
	`);
	fillMovies(strSearch, 1);

	$(document).on(
		'click',
		'#pagination_4 li.current-page:not(.active)',
		function() {
			return fillMovies(strSearch, +$(this).text());
		}
	);
	$('#next-page-4').on('click', function() {
		return fillMovies(strSearch, currentPageTopRated + 1);
	});

	$('#previous-page-4').on('click', function() {
		return fillMovies(strSearch, currentPageTopRated - 1);
	});
	$('#pagination_4').on('click', function() {
		$('html,body').animate({ scrollTop: 0 }, 400);
	});
}

async function fillMovies(strSearch, whichPage) {
	const reqStr = `${baseURL}search/movie/?api_key=${apiKey}&query=${strSearch}&page=${whichPage}`;
	const response = await fetch(reqStr);
	const movies = await response.json();

	if (movies.total_results === 0) {
		$('#main').empty();
		$('#main').append(`
		<div class="px-3">
			<h5>Không tìm thấy kết quả cho : "${strSearch}"</h5>
		</div>
		`);
		return true;
	}
	currentPage = whichPage;

	showMoviesSearchingByName(movies);
}

async function showMoviesSearchingByName(movies) {
	$('#movies-searching-by-name').empty();
	let lengthMovie, imgUrl;
	for (const m of movies.results) {
		// lengthMovie = await getLengthOfMovie(m.id);
		if (m === undefined)
			imgUrl = `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw5WrlkUQT9BMeXPL4NVAwuD1n9hqczs__ff8Uv8XJnPfau98e&s`;
		else {
			if (m.poster_path === null) {
				imgUrl = `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw5WrlkUQT9BMeXPL4NVAwuD1n9hqczs__ff8Uv8XJnPfau98e&s`;
			} else {
				imgUrl = `${baseImageURL}w500/${m.poster_path}`;
			}
		}
		$('#movies-searching-by-name').append(
			`<div class="col-md-4 py-1 mt-2">
				<div class="card shadow h-100" onclick="movieDetails(${m.id})">
					<div class="about-picture">
						<img
							class="card-img-top"
							src="${imgUrl}"
							alt="Card image cap"
						/>
						<p class="picture-title">
							<i class="fa fa-play fa" aria-hidden="true"></i>
						</p>
					</div>
					
					<div class="card-body ">
						<h5 class="card-title "><a class="text-dark" href="#">${m.title}</a></h5>
						<div class="row">
						<div class="col-7">
						<p class="text-secondary pb-0 mb-0"> <span class="fa fa-star checked"></span> ${m.vote_average}</p>
						</div>
						<div class="col-5">
						<p class="text-secondary pb-0 mb-0 pl-3">
						<i class="fa fa-calendar pr-2" aria-hidden="true"></i>
						${m.release_date} </p></div>
					</div>
						
				</div>
			</div>`
		);
	}
	// Xử lí cho phần pagination
	// Replace the navigation items (not prev/next):
	$('#pagination_4 li')
		.slice(1, -1)
		.remove();
	getPageList(movies.total_pages, currentPage, movies.results.length).forEach(
		item => {
			$('<li>')
				.addClass('page-item')
				.addClass(item ? 'current-page' : 'disabled')
				.toggleClass('active', item === currentPage)
				.append(
					$('<a>')
						.addClass('page-link')
						.attr({
							href: 'javascript:void(0)'
						})
						.text(item || '...')
				)
				.insertBefore('#next-page-4');
		}
	);
	// Disable prev/next when at first/last page:
	$('#previous-page-4').toggleClass('disabled', currentPage === 1);
	$('#next-page-4').toggleClass(
		'disabled',
		currentPage === movies.total_pages
	);
}

// ---------------KẾT THÚC TÌM KIẾM MOVIE THEO TÊN MOVIE VÀ PHÂN TRANG ----------------//

// ------------- CHỨC NĂNG TÌM KIẾM MOVIE THEO DIỄN VIÊN VÀ PHÂN TRANG -------//

//-----------------------------------------------------------------------------------//
// Đầu tiên sẽ tìm các diễn viên có tên match với stringSearch. (1)					 //
// Sau đó với mỗi diễn viên sẽ get tất cả phim mà diễn viên đó có tham gia           //
// Kết quả cần hiển thị là tất cả các phim của tất cả diễn viên ở (1).               //
//-----------------------------------------------------------------------------------//
let castsId = []; // mảng Id của diễn viên có tên match với strSearch
let arrMoviesId = []; // mảng Id các phim của tất cả diễn viên trên - 1 id diễn viên sẽ tương ứng
let setMoviesId = null; // dùng để chứa list các id không trùng nhau - Vì 2 diễn viên có thể đóng chung 1 bộ phim
let flag = 0;

function searchMoviesByCast(strSearch) {
	getCastsIdBySearching(strSearch);
}

async function getCastsIdBySearching(strSearch) {
	let reqStr = `${baseURL}search/person?api_key=${apiKey}&query=${strSearch}`;
	let response = await fetch(reqStr);
	let rs = await response.json();

	if (rs.total_results === 0) {
		$('#main').empty();
		$('#main').append(`
		<div class="px-3">
			<h5>Không tìm thấy kết quả cho : "${strSearch}"</h5>
		</div>
		`);
		return true;
	}

	let id_popularity_cast = 0;

	id_popularity_cast = rs.results.find(function(item) {
		return item.known_for_department == 'Acting';
	}).id;
	getMoviesBySearchingCast(id_popularity_cast);
}

async function getMoviesBySearchingCast(id) {
	// Request để lấy movies của diễn viên theo id diễn viên
	const reqStr = `${baseURL}person/${id}/movie_credits?api_key=${apiKey}`;
	const response = await fetch(reqStr);
	const rs = await response.json();

	//  Gán array movies của diễn viên vào biến toàn cục moviesOfCast để dùng cho pagination
	moviesOfCast.splice(0, moviesOfCast.length);
	moviesOfCast = rs.cast;

	$('#main').empty();
	$('#main').append(`
		<div id="movies-searching-by-cast" class="row">
			<!-- Nơi in ra list movies searching by cast-->
		</div>
		<nav aria-label="Page navigation example" class="w-100 mt-4">
			<ul class="pagination justify-content-center" id="pagination_5">
				<li class="page-item disabled" id="previous-page-5">
					<a
						class="page-link"
						href="javascript:void(0)"
						tabindex="-1"
						aria-disabled="true"
						>Prev</a
					>
				</li>
				<li class="page-item " id="next-page-5">
					<a class="page-link" href="javascript:void(0)">Next</a>
				</li>
			</ul>
		</nav>
	`);

	// Lấy số phim của diễn viên để phân trang
	let totalMovies = rs.cast.length;
	// console.log(totalMovies);

	// Từ đó suy ra số trang
	totalPages = Math.ceil(totalMovies / movies_per_page);
	console.log(totalPages);

	// Hiên thị movies trong trang 1 của phân trang
	showMoviesBySearchingCast(1);

	// Use event delegation, as these items are recreated later
	$(document).on(
		'click',
		'#pagination_5 li.current-page:not(.active)',
		function() {
			return showMoviesBySearchingCast(+$(this).text());
		}
	);
	$('#next-page-5').on('click', function() {
		return showMoviesBySearchingCast(currentPage + 1);
	});

	$('#previous-page-5').on('click', function() {
		return showMoviesBySearchingCast(currentPage - 1);
	});

	$('#pagination_5').on('click', function() {
		$('html,body').animate({ scrollTop: 0 }, 500);
	});
}

async function showMoviesBySearchingCast(whichPage) {
	if (whichPage < 1 || whichPage > totalPages) return false;
	currentPage = whichPage;
	let imgUrl;
	let genresAMovie = [];
	let lengthMovie;
	$('#movies-searching-by-cast').empty();
	// Xử lí show các movies theo whichpage (là trang của phân trang)

	for (let index = 0; index < 6; index++) {
		if (index + 6 * (whichPage - 1) === moviesOfCast.length) break;

		let m = moviesOfCast[index + movies_per_page * (whichPage - 1)];

		// lengthMovie = await getLengthOfMovie(m.id);

		if (m === undefined)
			imgUrl = `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw5WrlkUQT9BMeXPL4NVAwuD1n9hqczs__ff8Uv8XJnPfau98e&s`;
		else {
			if (m.poster_path === null) {
				imgUrl = `https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw5WrlkUQT9BMeXPL4NVAwuD1n9hqczs__ff8Uv8XJnPfau98e&s`;
			} else {
				imgUrl = `${baseImageURL}w500/${m.poster_path}`;
			}
		}
		// genresAMovie = await getGenresOfMovie(m.id);
		$('#movies-searching-by-cast').append(`
		<div class="col-md-4 py-1 mt-2">
			<div class="card shadow h-100" onclick="movieDetails(${m.id})">
				<div class="about-picture">
					<img
						class="card-img-top"
						src="${imgUrl}"
						alt="Card image cap"
					/>
					<p class="picture-title">
						<i class="fa fa-play fa" aria-hidden="true"></i>
					</p>
				</div>
				
				<div class="card-body ">
					<h5 class="card-title "><a class="text-dark" href="#">${m.title}</a></h5>
					<div class="row">
						<div class="col-7">
						<p class="text-secondary pb-0 mb-0"> <span class="fa fa-star checked"></span> ${m.vote_average}</p></div>
						<div class="col-5">
						<p class="text-secondary pb-0 mb-0 pl-3">
						<i class="fa fa-calendar pr-2" aria-hidden="true"></i>
						${m.release_date} </p></div>
					</div>
					
					
					
				</div>	
			</div>
		</div>`);
	}

	$('#pagination_5 li')
		.slice(1, -1)
		.remove();
	var arr = getPageList(totalPages, currentPage, 7);

	arr.forEach(item => {
		$('<li>')
			.addClass('page-item')
			.addClass(item ? 'current-page' : 'disabled')
			.toggleClass('active', item === currentPage)
			.append(
				$('<a>')
					.addClass('page-link')
					.attr({
						href: 'javascript:void(0)'
					})
					.text(item || '...')
			)
			.insertBefore('#next-page-5');
	});
	// Disable prev/next when at first/last page:
	$('#previous-page-5').toggleClass('disabled', currentPage === 1);
	$('#next-page-5').toggleClass('disabled', currentPage === totalPages);

	return true;
}

async function getLengthOfMovie(id) {
	const reqStr = `${baseURL}movie/${id}?api_key=${apiKey}`;
	const response = await fetch(reqStr);
	const rs = await response.json();
	return rs.runtime;
}

// ---------------KẾT THÚC TÌM KIẾM MOVIE THEO DIỄN VIÊN VÀ PHÂN TRANG ----------------//

// ---------- CHỨC NĂNG CHI TIẾT MOVIES VÀ PHÂN TRANG DANH SÁCH DIỄN VIÊN CỦA PHIM ----------//
let listCastsOfMovie = [];
let currentListCastsMoviePage = 0;
let totalCastsOfMovie = 0;
let totalCastPage = 0;

async function movieDetails(id) {
	modalLoaing();
	const reqStr = `${baseURL}movie/${id}?api_key=${apiKey}`;
	const response = await fetch(reqStr);
	const rs = await response.json();
	// Lấy các thể loại của phim

	let genresMovie = await getGenresOfMovie(id);

	// Lấy các công ty sản xuất của phim
	let productionCompanies = [];
	rs.production_companies.forEach(entry => {
		productionCompanies.push(entry.name);
	});

	// Lấy các quốc gia sản xuất của phim
	let productionCountries = [];
	rs.production_countries.forEach(entry => {
		productionCountries.push(entry.name);
	});

	// Kiểm tra ảnh poster có null không, nếu có thì thay bằng ảnh mặc định
	let img = ``;
	if (rs.poster_path == null) {
		img = `http://www.thebristolarms.com.au/wp-content/uploads/2018/03/img-not-found.png`;
	} else {
		img = `${baseImageURL}w500/${rs.poster_path}`;
	}

	// Lấy các đạo diễn của phim
	let directors = [];
	directors = await getDirectors(id);

	// Lấy reviews của movie
	const reviews = await getReviewsMovie(id);
	// console.log('Director: ' + directors.join(', '));
	const castsOfMovie = await getCastsOfAMovie(id);
	$('#main').empty();

	$('html,body').animate({ scrollTop: 0 }, 500);
	$('#main').append(`
    <div class="row py-2 ml-5 mt-4">
				<div class="col-sm-4">
					<div class="card shadow h-100">
						<div class="about-picture">
							<img
								class="card-img-top"
								src="${img}"
								alt="Card image cap"
							/>
							<p class="picture-title">
								<i class="fa fa-play fa" aria-hidden="true"></i>
							</p>
						</div>

						<div class="card-body h-10">
							<ul class="social-icons mt-0 mb-0">
								<li><i class="fab fa-facebook-square"></i></li>
								<li><i class="fab fa-twitter-square"></i></li>
								<li><i class="fab fa-instagram"></i></li>
								<li>
									<i class="fab fa-google-plus-square"></i>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-6 text-left offset-top-30 ml-5">
					<h2>${rs.original_title}</h2>
					<div class="review-2 row">
						<div class="col-4">
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
							<span class="fa fa-star checked"></span>
						</div>
						<div class="col-6">
							
						</div>
					</div>
					<br />
					<p>
						${rs.overview}
					</p>

					<br />
					<dl class="row">
						<dt class="col-sm-3">Đạo diễn:</dt>
						<dd class="col-sm-9">
							${directors.join(', ')}
						</dd>
						<dt class="col-sm-3">Thể loại:</dt>
						<dd class="col-sm-9">
							${genresMovie.join(', ')}
						</dd>

						<dt class="col-sm-3">Ngày phát hành:</dt>
						<dd class="col-sm-9">
							${rs.release_date}
						</dd>

						<dt class="col-sm-3">Thời lượng:</dt>
						<dd class="col-sm-9">
							${rs.runtime} min
						</dd>

						<dt class="col-sm-3 text-truncate">
							Quốc gia:
						</dt>
						<dd class="col-sm-9">
							${productionCountries.join(', ')}
						</dd>

						<dt class="col-sm-3">Nhà sản xuất:</dt>
						<dd class="col-sm-9">
							<dl class="row px-3">
								${productionCompanies.join(', ')}
							</dl>
						</dd>
                        <dt class="col-sm-6"></dt>
                        
                    </dl>
                    <div>
                        <i class="fab fa-imdb fa-3x rounded ">
                            <span class="imdbscore"> : ${
								rs.vote_average
							}&nbsp</span>
                        </i>
                    </div>
				</div>
			</div>
            
			<div class="container mt-5 ml-4">
                <h2>Diễn viên: </h2>
                <hr />
				<div class="row py-2" id="casts-of-movie">
					<!-- Nơi show các diễn viên của phim -->
				</div>
				<nav aria-label="Page navigation example" class="w-100">
					<ul class="pagination justify-content-center" id="pagination_3">
						<li class="page-item disabled" id="previous-page-3">
							<a
								class="page-link"
								href="javascript:void(0)"
								tabindex="-1"
								aria-disabled="true"
								>Prev</a
							>
						</li>
						<li class="page-item " id="next-page-3">
							<a class="page-link" href="javascript:void(0)">Next</a>
						</li>
					</ul>
				</nav>
			${reviews}
	`);
	await getCastsOfAMovie(id);
}

// Hàm lấy các thể loại của 1 movie theo id movie
async function getGenresOfMovie(id) {
	const reqStr = `${baseURL}movie/${id}?api_key=${apiKey}`;
	const response = await fetch(reqStr);
	const rs = await response.json();
	// Lấy các thể loại của phim
	let genresMovie = [];
	rs.genres.forEach(entry => {
		genresMovie.push(entry.name);
	});
	return genresMovie;
}

// Hàm lấy các diễn viên của 1 movie theo id movie
async function getCastsOfAMovie(id) {
	// Lay thong tin ve dien vien cua phim
	const reqStrCast = `${baseURL}movie/${id}/credits?api_key=${apiKey}`;
	const responseCast = await fetch(reqStrCast);
	const rsCast = await responseCast.json();

	// Lấy mảng các diễn viên của phim
	listCastsOfMovie = rsCast.cast;

	totalCastsOfMovie = listCastsOfMovie.length;

	totalCastPage = Math.ceil(totalCastsOfMovie / 6); // 5 diễn viên trong 1 trang
	showListCastsOfMovie(1);

	$(document).on(
		'click',
		'#pagination_3 li.current-page:not(.active)',
		function() {
			return showListCastsOfMovie(+$(this).text());
		}
	);
	$('#next-page-3').on('click', function() {
		return showListCastsOfMovie(currentListCastsMoviePage + 1);
	});

	$('#previous-page-3').on('click', function() {
		return showListCastsOfMovie(currentListCastsMoviePage - 1);
	});
}

function showListCastsOfMovie(whichPage) {
	if (whichPage < 1 || whichPage > totalCastPage) return false;
	currentListCastsMoviePage = whichPage;

	$('#casts-of-movie').empty();
	// Xử lí show các movies theo whichpage (là trang của phân trang)

	for (let index = 0; index < 6; index++) {
		if (index + 6 * (whichPage - 1) === listCastsOfMovie.length) break;

		let c = listCastsOfMovie[index + movies_per_page * (whichPage - 1)];

		let imgUrl = ``;
		if (c.profile_path === null) {
			imgUrl = `img/no avatar.jpg`;
		} else {
			imgUrl = `${baseImageURL}w92/${c.profile_path}`;
		}
		$('#casts-of-movie').append(`
		<div class="col-2">
			<div >
				<img src="${imgUrl}" alt="" />

				<div class="mt-2">
					<h6 ><strong> 
					<a href="javascript:void(0)" onclick="castDetails(${c.id})" class="text-dark btn px-0"> ${c.name}
					</a>
					</strong></h6>
				</div>

				<div>
					<p class="text-secondary">vai ${c.character}</p>
				</div>
			</div>
		</div>
			`);
	}

	// Xử lí cho phần pagination
	// Replace the navigation items (not prev/next):
	$('#pagination_3 li')
		.slice(1, -1)
		.remove();
	var arr = getPageList(totalCastPage, currentListCastsMoviePage, 6);

	arr.forEach(item => {
		$('<li>')
			.addClass('page-item')
			.addClass(item ? 'current-page' : 'disabled')
			.toggleClass('active', item === currentListCastsMoviePage)
			.append(
				$('<a>')
					.addClass('page-link')
					.attr({
						href: 'javascript:void(0)'
					})
					.text(item || '...')
			)
			.insertBefore('#next-page-3');
	});
	// Disable prev/next when at first/last page:
	$('#previous-page-3').toggleClass(
		'disabled',
		currentListCastsMoviePage === 1
	);
	$('#next-page-3').toggleClass(
		'disabled',
		currentListCastsMoviePage === totalCastPage
	);
	return true;
}

// Hàm lấy các đạo diễn của phim
async function getDirectors(id) {
	const reqStrDir = `${baseURL}movie/${id}/credits?api_key=${apiKey}`;
	const responseDir = await fetch(reqStrDir);
	const rsDir = await responseDir.json();
	var directors = [];
	rsDir.crew.forEach(function(entry) {
		if (entry.job === 'Director') {
			directors.push(entry.name);
		}
	});
	return directors;
}

// ---------------KẾT THÚC MOVIE DETAILS VÀ PHÂN TRANG----------------------//

// ------------------- CHỨC NĂNG CAST DETAILS VÀ PHÂN TRANG DANH SÁCH MOVIES CỦA DIỄN VIÊN--------------------//

// ---- Các biến toàn cục dùng cho phân trang trong CastDetails
let moviesOfCast = []; // chứa toàn bộ movies của diễn viên
let totalPages = 0; // tổng số trang
let page = 0; // trang hiên tại
let maxLength = 0; // số page-item tối đa hiển thị trong phân trang. Dùng cho trường hợp totalPages lớn.
const movies_per_page = 6; // số movies hiển thị trên 1 trang
let currentPage = 0;

// Hàm xem thông tin chi tiết diễn viên (liên kết từ xem chi tiết movie)
async function castDetails(id) {
	modalLoaing();
	// Request để lấy thông tin diễn viên theo id
	const reqStr = `${baseURL}person/${id}?api_key=${apiKey}`;
	const response = await fetch(reqStr);
	const rs = await response.json();

	// const moviesOfCast = await getMoviesOfCast(id);
	$('#main').empty();
	$('html,body').animate({ scrollTop: 0 }, 500);
	$('#main').append(`
	<div class="col-4 px-2 rounded shadow border-top" style="background-color:#f6f9fc";>
		<div class="mt-3">
			<div class="text-center">
				<img
					class="rounded-circle avatar-cast shadow"
					src="${baseImageURL}w500/${rs.profile_path}"
					alt="Card image cap"
				/>
			</div>

			<div class=" mt-3">
				<!-- <ul class="social-icons mt-0 mb-0">
							<li>
								<i class="fab fa-facebook-square"></i>
							</li>
							<li>
								<i class="fab fa-twitter-square"></i>
							</li>
							<li><i class="fab fa-instagram"></i></li>
							<li>
								<i
									class="fab fa-google-plus-square"
								></i>
							</li>
						</ul> -->
				<!-- <h3 class="text-justify">Leonardo DiCaprio</h3> -->
				<blockquote class="text-center w-75 mx-auto">
					<h4>
						${rs.name}
					</h4>

					<cite title="Source Title"
						>Diễn viên
						<i
							class="glyphicon glyphicon-map-marker"
						></i
					></cite>
				</blockquote>
			</div>
		</div>
		<div class="pl-2">
			<div class="review-2 mx-auto text-center">
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
				<span class="fa fa-star checked"></span>
			</div>
			<br />

			<br />
			<dl class="row pl-2">
				<dt class="col-sm-3 pr-0">Tiểu sử:</dt>
				<dd class="col-sm-9 pl-0">
					${rs.biography}
				</dd>

				<dt class="col-sm-3 pr-0">Ngày sinh:</dt>
				<dd class="col-sm-9 pl-0">
					${rs.birthday}
				</dd>

				<dt class="col-sm-3 pr-0">Giới tính:</dt>
				<dd class="col-sm-9 pl-0">
					${rs.gender == 2 ? 'Nam' : 'Nữ'}
				</dd>

				<dt class="col-sm-3 pr-0">Nơi sinh:</dt>
				<dd class="col-sm-9 pl-0">
					${rs.place_of_birth}
				</dd>
			</dl>
		</div>
	</div>
	`);
	await getMoviesOfCast(id);
}

// Hàm lấy các movie mà diễn viên đã tham gia theo id diễn viên
async function getMoviesOfCast(id) {
	// Request để lấy movies của diễn viên theo id diễn viên
	const reqStr = `${baseURL}person/${id}/movie_credits?api_key=${apiKey}`;
	const response = await fetch(reqStr);
	const rs = await response.json();

	//  Gán array movies của diễn viên vào biến toàn cục moviesOfCast để dùng cho pagination
	moviesOfCast = [];
	moviesOfCast = rs.cast;

	$('#main').append(`
	<div class="col-8">
		<h2 class ="ml-4"><strong>Danh sách phim đã tham gia</strong></h2>
		<hr />
		<div id="movies_of_cast">
			<!- Nơi chứa list movies of cast -->
		</div>
		<nav aria-label="Page navigation example" class="w-100">
			<ul class="pagination justify-content-center" id="pagination_2">
				<li class="page-item disabled" id="previous-page-1">
					<a
						class="page-link"
						href="javascript:void(0)"
						tabindex="-1"
						aria-disabled="true"
						>Prev</a
					>
				</li>
				<li class="page-item " id="next-page-1">
					<a class="page-link" href="javascript:void(0)">Next</a>
				</li>
			</ul>
		</nav>
	</div>`);

	// Lấy số phim của diễn viên để phân trang
	let totalMovies = rs.cast.length;
	// console.log(totalMovies);

	// Từ đó suy ra số trang
	totalPages = Math.ceil(totalMovies / movies_per_page);
	console.log(totalPages);

	// Hiên thị movies trong trang 1 của phân trang
	showMoviesOfCast(1);

	// Use event delegation, as these items are recreated later
	$(document).on(
		'click',
		'#pagination_2 li.current-page:not(.active)',
		function() {
			return showMoviesOfCast(+$(this).text());
		}
	);
	$('#next-page-1').on('click', function() {
		return showMoviesOfCast(currentPage + 1);
	});

	$('#previous-page-1').on('click', function() {
		return showMoviesOfCast(currentPage - 1);
	});

	$('#pagination_2').on('click', function() {
		$('html,body').animate({ scrollTop: 0 }, 500);
	});
}

// Hàm xử lí phân trang khi có nhiều page
function getPageList(totalPages, page, maxLength) {
	if (maxLength < 5) throw 'maxLength must be at least 5';

	function range(start, end) {
		return Array.from(Array(end - start + 1), (_, i) => i + start);
	}

	var sideWidth = maxLength < 9 ? 1 : 2;
	var leftWidth = (maxLength - sideWidth * 2 - 3) >> 1;
	var rightWidth = (maxLength - sideWidth * 2 - 2) >> 1;
	if (totalPages <= maxLength) {
		// no breaks in list
		return range(1, totalPages);
	}
	if (page <= maxLength - sideWidth - 1 - rightWidth) {
		// no break on left of page
		return range(1, maxLength - sideWidth - 1)
			.concat([0])
			.concat(range(totalPages - sideWidth + 1, totalPages));
	}
	if (page >= totalPages - sideWidth - 1 - rightWidth) {
		// no break on right of page
		return range(1, sideWidth)
			.concat([0])
			.concat(
				range(
					totalPages - sideWidth - 1 - rightWidth - leftWidth,
					totalPages
				)
			);
	}
	// Breaks on both sides
	return range(1, sideWidth)
		.concat([0])
		.concat(range(page - leftWidth, page + rightWidth))
		.concat([0])
		.concat(range(totalPages - sideWidth + 1, totalPages));
}

// Hàm in movies của diễn viên theo từng trang trong phân trang
async function showMoviesOfCast(whichPage) {
	if (whichPage < 1 || whichPage > totalPages) return false;
	currentPage = whichPage;
	let imgUrl;
	let genresAMovie = [];
	$('#movies_of_cast').empty();
	// Xử lí show các movies theo whichpage (là trang của phân trang)

	for (let index = 0; index < movies_per_page; index++) {
		if (index + movies_per_page * (whichPage - 1) === moviesOfCast.length)
			break;
		let m = moviesOfCast[index + movies_per_page * (whichPage - 1)];
		if (m === undefined) imgUrl = `img/img-not-found.jpg`;
		else {
			if (m.backdrop_path === null) {
				imgUrl = `img/img-not-found.jpg`;
			} else {
				imgUrl = `${baseImageURL}w300/${m.backdrop_path}`;
			}
		}
		// genresAMovie = await getGenresOfMovie(m.id);
		$('#movies_of_cast').append(`
			<div class="row py-2 mb-3 pl-3">
				<div class="col-5 mr-2 ml-2">
					<img
						class="rounded"
						src="${imgUrl}"
						style="width: 300px;"
					/>
				</div>
				<div class="col-6 info-movie ">
					<div class="mt-2 pl-2">
						<h4>
							<strong
								><a href="#" class="text-dark"
									>${m.title}</a
								></strong
							>
						</h4>
						<span class="fa fa-star checked"></span> :
						${m.vote_average}/10
						<dl class="row mt-2 mb-0">
							<dt class="col-sm-5 pr-0">
								Ngày phát hành :
							</dt>
							<dd class="col-sm-7 px-0">
								${m.release_date}
							</dd>
							<dt class="col-sm-5 pr-0">Số lượng vote:</dt>
							<dd class="col-sm-7 px-0">
								${m.vote_count}
							</dd>
						</dl>
						<p
							class="font-italic text-right text-secondary"
						>
							... đóng vai ${m.character}
						</p>
					</div>
				</div>
			</div>`);
	}
	// Xử lí cho phần pagination
	// Replace the navigation items (not prev/next):
	$('#pagination_2 li')
		.slice(1, -1)
		.remove();
	var arr = getPageList(totalPages, currentPage, movies_per_page);

	arr.forEach(item => {
		$('<li>')
			.addClass('page-item')
			.addClass(item ? 'current-page' : 'disabled')
			.toggleClass('active', item === currentPage)
			.append(
				$('<a>')
					.addClass('page-link')
					.attr({
						href: 'javascript:void(0)'
					})
					.text(item || '...')
			)
			.insertBefore('#next-page-1');
	});
	// Disable prev/next when at first/last page:
	$('#previous-page-1').toggleClass('disabled', currentPage === 1);
	$('#next-page-1').toggleClass('disabled', currentPage === totalPages);

	return true;
}
// ----------------- KẾT THÚC CHỨC NĂNG CAST DETAILS VÀ PHÂN TRANG ------------------//

// ---------------------- CHỨC NĂNG REVIEW MOVIES ---------------------//
// Hàm lấy review của 1 movie theo id movie
async function getReviewsMovie(id) {
	const reqStr = `${baseURL}movie/${id}/reviews?api_key=${apiKey}`;
	const response = await fetch(reqStr);
	const rs = await response.json();
	let result = `
	<div class="row py-2">
	<div class="col"><hr /></div>
	<div class="col-auto"><h2>Reviews</h2></div>
	<div class="col"><hr /></div>
	`;
	for (const r of rs.results) {
		result += `
		<div class="card mb-2 w-100">
		<div class="card-body">
		<div class="row">
			<div class="col-md-2">
				<img
					src="https://image.ibb.co/jw55Ex/def_face.jpg"
					class="img img-rounded img-fluid"
				/>
				<p class="text-secondary text-center">
					ID: ${r.id}
				</p>
			</div>
			<div class="col-md-10">
				<p>
					<a
						href="https://maniruzzaman-akash.blogspot.com/p/contact.html"
						><strong>${r.author}</strong></a
					>
				</p>
				<p>
					${r.content}
				</p>
				<p>
					<a
						class="float-right btn btn-outline-primary ml-2"
					>
						<i class="fa fa-reply"></i> Reply</a
					>
					<a
						class="float-right btn text-white btn-danger"
					>
						<i class="fa fa-heart"></i> Like</a
					>
				</p>
			</div>
		</div>
	</div>
</div>`;
	}
	result += `</div>`;
	return result;
}

// -------------------------- END ------------------------------------------//
