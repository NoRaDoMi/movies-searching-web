
#


# Đồ án Giữa kì : PHÁT TRIỂN ỨNG DỤNG WEB

### Ứng dụng tìm kiếm thông tin phim, diễn viên sử dụng HTML,CSS,Jquery,Bootstrap và WEB API (TheMovieDB API)

```
 Thực hiện: Võ Trọng Phúc 
 MSSV:  1712126.
```

### 1\. Tổng quan về ứng dụng

#### 1.1. Các chức năng

Ứng dụng bao gồm các chức năng:

* Tìm kiếm phim theo tên, theo diễn viên 
* Cho phép xem chi tiết thông tin phim (hình ảnh, đạo diễn, diễn viên, tóm tắt, năm phát
hành,…) và reviews của phim
* Cho phép xem thông tin diễn viên.
* Phân trang

#### 1.2. Các giao diện chức năng 
##### 1.2.1 `Giao diện chung`: homepage sẽ hiển thị top rated movies
![giao dien chung](https://user-images.githubusercontent.com/38036797/68081654-488de200-fe44-11e9-953a-9ff76ae98b34.PNG)

##### 1.2.2`Chức năng tìm kiếm phim theo tên:` vd: Harry Potter
![tktheophim](https://user-images.githubusercontent.com/38036797/68081728-39f3fa80-fe45-11e9-8e6d-89fe57eadc65.png)

##### 1.2.3`Chức năng tìm kiếm phim theo diễn viên:` vd: Ngô Thanh Vân
![tktheodv](https://user-images.githubusercontent.com/38036797/68081762-a40c9f80-fe45-11e9-8271-4e4ddfa5b596.png)

##### 1.2.4`Chức năng xem chi tiết movies:` vd : Your Name
![chitietphim1](https://user-images.githubusercontent.com/38036797/68081799-1f6e5100-fe46-11e9-97a1-978288a8f9f7.PNG)
![chitietphim2](https://user-images.githubusercontent.com/38036797/68081805-2e550380-fe46-11e9-810d-f142b33e4ed7.PNG)
![chitietphim3](https://user-images.githubusercontent.com/38036797/68081807-42006a00-fe46-11e9-8a26-661fcfff416f.PNG)

##### 1.2.5`Chức năng xem thông tin diễn viên:` bằng cách click vào tên diễn viên trong danh sách diễn viên của phim đó.
![chitietdv1](https://user-images.githubusercontent.com/38036797/68081834-c2bf6600-fe46-11e9-9063-1bf673bf906d.PNG)
![chitietdv2](https://user-images.githubusercontent.com/38036797/68081839-ce129180-fe46-11e9-8264-d84a84b2f8bb.PNG)

##### 1.2.6`Phân trang cho page:` 
![pt-toprated](https://user-images.githubusercontent.com/38036797/68081887-b2f45180-fe47-11e9-9dc0-684262c12e03.PNG)
d84a84b2f8bb.PNG)
![pt-movies](https://user-images.githubusercontent.com/38036797/68081886-b2f45180-fe47-11e9-83d0-e220940c6090.PNG)

![dienvien](https://user-images.githubusercontent.com/38036797/68081883-b25bbb00-fe47-11e9-93ac-7c1a95767847.PNG)
![phantrang1-mc](https://user-images.githubusercontent.com/38036797/68081884-b25bbb00-fe47-11e9-8306-3331491c0da6.PNG)
![pt-dv](https://user-images.githubusercontent.com/38036797/68081885-b2f45180-fe47-11e9-8890-4b4b418eb663.PNG)

##### 1.2.7`Chức năng hiển thị reviews movies: `
![reviews](https://user-images.githubusercontent.com/38036797/68081903-fa7add80-fe47-11e9-8dc8-1cbb582437c8.PNG)

##### `*** Thông báo khi không tìm thấy kết quả:`
![loi](https://user-images.githubusercontent.com/38036797/68081957-cf44be00-fe48-11e9-9ed3-3f6310c9ebae.PNG)

##### `*** Loading khi search hoặc xem thông tin chi tiết phim, diễn viên`
![Loading](https://user-images.githubusercontent.com/38036797/68082001-8b9e8400-fe49-11e9-9d3e-126939c53847.png)